import React from "react";
import data from "../data/cricket.json";
import PropTypes from 'prop-types';
import './Cricket.css';
import moment from "moment";

function Cricket() {

    const matchDate = moment(data.data.sport_event.scheduled).format("DD MMMM YYYY");
        // console.log('Data', data.data.sport_event.season.name)
 
    return(
        <>
        <br /><br />
        <section className="section">
            <div className="container">
                <div className="row">
                    <div className="title-left-side">
                        <p><a href="#" className="light-text">{data.data.sport_event.season.name}</a></p>
                        <h1>{data.data.sport_event.competitors[0].name} VS {data.data.sport_event.competitors[1].name}</h1>
                    </div>
                    <div className="title-right-side">
                        <span className="text">{data.data.sport_event.venue.name}, {data.data.sport_event.venue.city_name} | {matchDate} </span>
                        <p>{data.data.sport_event_status.match_result}</p>
                    </div>
                </div>
            </div>
        </section>
        </>
    )
      
}

Cricket.propTypes = {
    name: PropTypes.string
  };

export default Cricket;