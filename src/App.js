import React from 'react';
import './App.css';
import Cricket from './components/cricket';

function App() {
  return (
    <div className="App">
      <Cricket />
    </div>
  );
}

export default App;
